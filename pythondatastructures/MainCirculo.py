"""
Ejemplo de uso de la clase para crear objetos y llamar a métodos
@author. Jazna Meza Hidalgo
"""

from Circulo import Circulo
# Crea un objeto
cx = Circulo()
"""
	La linea anterior muestra el uso del constructor sin argumentos para mostrar 
	el uso de los valores por defecto de los parámetros
"""
print(cx.toString())
print(cx)
cy = Circulo(3.5,"azul")
print(cy.toString())
print(cy)
dir(cy)