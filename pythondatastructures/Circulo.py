class Circulo(object):
    def __init__(self, radio=2,color="rojo"):
        self.radio = radio
        self.color = color
    def toString(self):
        return "Circulo de color : " + self.color + " con radio " + str(self.radio)
    def __str__(self):
        return "* Circulo de color : " + self.color + " con radio " + str(self.radio) + " *"