#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 10:09:26 2017

@author: GhostGirl
"""

"""
Cree una variable x que contenga una lista de números enteros consecutivos entre el 1 y el 50 
(es decir 1,2,3,...,49,50). Use la funcion str() y sus métodos para crear un string que 
corresponda a la unión de los valores de la lista x y que se use el símbolo - para unir los 
números (es decir queremos obtener un string tipo '1-2-3-....-49-50').
"""
c=list()
for i in range(1,51):
    c.append(str(i))
y = "-".join(c)
print(y)

# Usando map para no crear la lista
c=map(str,range(1,51))
y = "-".join(c)
print(y)

"""
En el laboratorio usamos una raspberry que interactúa con un dispositivo GSM para recibir y 
generar llamadas usando la la red de celular. Cada vez que se recibe una llamada, el raspberry recibe las siguiente mensajería contenida en la variable mensaje.
Escriba un código de python que permita recorrer la lista y que obtenga el número telefónico 
que aparece justo después de la palabra +CLIP
"""
mensaje = ["", "RING", "", "", "","+CLIP: ""+56 22 3547451"",161,"""",0,"""",0"]
numero=mensaje[5]
print(numero[numero.find(":"):numero.find(",")])

# Otra forma para no dejar el índice fijo sería
indice = -1
for s in mensaje:
    if s.find("CLIP"):
        indice = mensaje.index(s)
if (indice != -1):
    numero=mensaje[indice]
    print(numero[numero.find(":"):numero.find(",")])

